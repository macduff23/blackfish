#!/bin/bash -e
CLEANUP=${CLEANUP:-0}
WORKDIR=${WORKDIR:-/home/core/src}

# SETUP CLOCK SOURCE
echo tsc | sudo tee -a /sys/devices/system/clocksource/clocksource0/current_clocksource > /dev/null

# SETUP ADDITIONAL DISKS
sudo mv $WORKDIR/{format-var-lib-docker.service,var-lib-docker.mount,format-var-lib-rkt.service,var-lib-rkt.mount} /etc/systemd/system/
sudo systemctl enable format-var-lib-docker.service
sudo systemctl enable var-lib-docker.mount
sudo systemctl enable format-var-lib-rkt.service
sudo systemctl enable var-lib-rkt.mount

#cleanup
if [[ "$CLEANUP" =~  [Y|y|1] ]]; then
    rm -Rf ~/.ssh/authorized_keys
fi
