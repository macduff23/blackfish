#!/bin/bash
source "$(dirname "$0")/functions.sh"
DATACENTER=${DATACENTER:-dc1}
LIB_DIR=/var/lib/flocker
CONF_DIR="$LIB_DIR"/etc

# Usage info
show_help() {
cat << EOF
Usage: ${0##*/} [-hv] start|stop|rm
Handles the lifecycle of the flocker agent service

COMMANDS:
    start              start flocker controller
    stop               stop flocker controller
    rm                 rm rkt container
OPTIONS:
    -h                 display this help and exit
    -v                 verbose mode. Can be used multiple
                       times for increased verbosity.
EOF
}

start() {
    sudo rkt run \
         --net=host \
         --volume resolv,kind=host,source=/etc/resolv.conf,readOnly=false \
         --mount volume=resolv,target=/etc/resolv.conf \
         --volume conf,kind=host,source="$CONF_DIR" \
         --volume proc,kind=host,source="/proc" \
         --volume root,kind=host,source="/" \
         --volume flocker,kind=host,source=/flocker \
         --volume dev,kind=host,source="/dev" \
         --volume bootid,kind=host,source=/proc/sys/kernel/random/boot_id \
         --mount volume=bootid,target=/proc/sys/kernel/random/boot_id \
         --mount volume=conf,target=/etc/flocker \
         --mount volume=root,target=/host \
         --mount volume=proc,target=/host/proc \
         --mount volume=flocker,target=/flocker \
         --mount volume=dev,target=/dev \
         --uuid-file-save "$UUID_FILE" \
         clusterhq/flocker-dataset-agent --exec /bin/sh -- -c '/usr/lib/klibc/bin/umount /proc/sys/kernel/random/boot_id && /tmp/wrap_dataset_agent_mtab.sh'
}

if [ ! -f /etc/blackfish/blackfish.conf ]; then
    log user.error "couldn't find configuration file"
    exit 1
fi

if [ "$VOLUME_DRIVER" != "flocker" ]; then
    log user.info "flocker is disabled"
    exit 0
fi
if [ "$SWARM_MODE" != "agent" ] && [ "$SWARM_MODE" != "both" ]; then
    log user.info "not a flocker agent node: nothing to do."
    exit 0
fi
if [ ! -f /etc/blackfish/flocker-agent.yml ]; then
    log user.error "flocker-agent.yml is missing"
    exit 1
fi

# /flocker cannot be created elsewhere. ... ask flockerz.
# the dataset agent "nsenters" the host filesystem and expects "/flocker" directory.
mkdir -p $LIB_DIR /flocker $CONF_DIR
cp /etc/blackfish/flocker-agent.yml $CONF_DIR/agent.yml
cp /etc/blackfish/certs/ca.pem "$CONF_DIR"/cluster.crt
cp /etc/blackfish/certs/api.pem "$CONF_DIR"/node.crt
cp /etc/blackfish/certs/api-key.pem "$CONF_DIR"/node.key

OPTIND=1 # Reset is necessary if getopts was used previously in the script.  It is a good idea to make this local in a function.
while getopts ":hv:" opt; do
    case "$opt" in
        h)
            show_help
            exit 0
            ;;
        v)  verbose=$((verbose+1))
            ;;
        '?')
            show_help >&2
            exit 1
            ;;
    esac
done
shift "$((OPTIND-1))" # Shift off the options and optional --.

case $1 in
    start)
        start
        ;;
    stop)
        stop_rkt
        ;;
    rm)
        rm_rkt
        ;;
    *)
        log user.error "unknown command: $1"
        exit 1
        ;;
esac
