#!/bin/bash
source "$(dirname "$0")/functions.sh"
log user.info "init blackfish"

# INSTALL TLS CERTS
if [ -f /etc/blackfish/certs/certs.tar ]; then
    cd /etc/blackfish/certs && tar -xf certs.tar
fi

cp /etc/blackfish/certs/ca.pem /etc/ssl/certs/"$DATACENTER.$STACK_NAME".pem
update-ca-certificates
mkdir -p /etc/docker/certs.d/registry.service.$STACK_NAME
cp /etc/blackfish/certs/ca.pem /etc/docker/certs.d/registry.service.$STACK_NAME

# INSTALL RKT IMAGES
pushd "$BLACKFISH_HOME"/acis
for i in ./*.aci; do
    if [ -f "$i" ]; then
        rkt fetch --insecure-options=image "$i"
    fi
done

# CONFIGURE NTP
ADMIN_IP=$(gethostadminipaddr)
if [ ! -z "$NTP_SERVERS" ]; then
    if echo "$NTP_SERVERS" | grep -qs "$ADMIN_IP"; then
        log user.info "node is a ntp server."

        tee /tmp/ntp.conf <<EOF
#Common Pool
server 0.coreos.pool.ntp.org iburst
server 1.coreos.pool.ntp.org iburst
server 2.coreos.pool.ntp.org iburst
server 3.coreos.pool.ntp.org iburst

#Server Pool
EOF
        for i in $(echo "$NTP_SERVERS" | sed 's/,/ /g'); do
            echo "peer $i iburst" >> /tmp/ntp.conf
        done

        echo "restrict  ${ADMIN_NETWORK%/*} mask $(cidr2mask ${ADMIN_NETWORK#*/})" >> /tmp/ntp.conf
    else
        tee /tmp/ntp.conf <<EOF
#Server Pool
EOF
        for i in $(echo "$NTP_SERVERS" | sed 's/,/ /g'); do
            echo "server $i iburst" >> /tmp/ntp.conf
        done

        echo "restrict  ${ADMIN_NETWORK%/*} mask $(cidr2mask ${ADMIN_NETWORK#*/})" >> /tmp/ntp.conf
    fi
    cp -f /tmp/ntp.conf /etc/ntp.conf
    systemctl stop systemd-timesyncd
    systemctl mask systemd-timesyncd
    systemctl enable ntpd
    systemctl start ntpd
fi

# DISABLE AUTO UPDATE
systemctl stop update-engine.service
systemctl stop locksmithd.service
systemctl mask update-engine.service
systemctl mask locksmithd.service
