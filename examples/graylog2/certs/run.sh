pushd /home/yann/work/ippon/blackfish/examples/graylog2/certs
echo '{"signing":{"default":{"expiry":"100000h","usages":["signing","server auth"]}}}' > req.json
if [ ! -z "graylog-api.service.datamc" ]; then
   echo '{"CN":"graylog-api","names":[{"OU":"datamc"}],"hosts":["graylog-api.service.datamc"],"key":{"algo":"rsa","size":2048}}' | cfssl gencert -ca=/home/yann/.blackfish/datamc/ca.pem -ca-key=/home/yann/.blackfish/datamc/ca-key.pem -config=req.json -hostname graylog-api.service.datamc - | cfssljson -bare "graylog-api"
else
   echo '{"CN":"graylog-api","names":[{"OU":"datamc"}],"key":{"algo":"rsa","size":2048}}' | cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=req.json - | cfssljson -bare "graylog-api"
fi
popd
