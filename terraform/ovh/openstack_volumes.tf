resource "openstack_blockstorage_volume_v2" "swarm_volumes" {
  name = "${var.stack_name}_bf_volume_${count.index}"
  size = "${var.swarm_volume_size}"
  count = "${var.swarm_volumes_count}"
}
