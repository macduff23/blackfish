resource "openstack_compute_instance_v2" "bastion" {
  name = "${var.stack_name}-bastion"
  image_id = "${openstack_image_v2.bastion.id}"
  flavor_id = "${lookup(var.bastion_flavor_id, var.ovh_region)}"
  key_pair = "${openstack_compute_keypair_v2.bf_keypair.name}"

  metadata {
    stack_name = "${var.stack_name}"
    blackfish_type = "bastion"
  }

  network {
    name = "${lookup(var.ovh_public_network_name, var.ovh_region)}"
  }

  network {
    uuid  = "${var.os_net_id}"
  }

  user_data = <<EOF
#cloud-config
write_files:
  - path: "/etc/stack.conf"
    permissions: "0644"
    owner: "root"
    content: |
      STACK_NAME="${var.stack_name}"
      ADMIN_NETWORK="${openstack_networking_subnet_v2.bf_subnet.cidr}"
      NAT_SUBNETS="${openstack_networking_subnet_v2.bf_subnet.cidr}"
EOF
}
