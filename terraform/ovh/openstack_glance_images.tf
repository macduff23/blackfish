resource "openstack_image_v2" "bastion" {
  name="${var.stack_name}-bf-bastion_image"
  local_file_path="${var.bastion_img_path}"
  container_format="bare"
  disk_format="qcow2"
}

resource "openstack_image_v2" "blackfish" {
  name="${var.stack_name}-bf-blackfish_image"
  local_file_path="${var.blackfish_img_path}"
  container_format="bare"
  disk_format="qcow2"
}
