provider "aws" {
    region = "${var.aws_region}"
}

resource "aws_iam_group" "blackfish" {
    name = "${var.stack_name}-blackfish"
    path = "/${var.stack_name}/"
}

resource "aws_iam_user" "blackfish" {
    name = "${var.stack_name}_blackfish"
    path = "/${var.stack_name}/users/"
}

resource "aws_iam_access_key" "blackfish_ak" {
    user = "${aws_iam_user.blackfish.name}"
}

resource "aws_iam_group_policy" "blackfish_policy" {
    name = "${var.stack_name}_blackfish_policy"
    group = "${aws_iam_group.blackfish.id}"
    policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "*",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_group_membership" "team_blackfish" {
    name = "tf-testing-group-membership"
    users = [
        "${aws_iam_user.blackfish.name}"
    ]
    group = "${aws_iam_group.blackfish.name}"
}

resource "aws_iam_access_key" "registry" {
    user = "${aws_iam_user.registry.name}"
}

resource "aws_iam_user" "registry" {
    name = "${var.stack_name}_docker_registry"
    path = "/${var.stack_name}/users/"
}

resource "aws_iam_user_policy" "registry" {
    name = "docker_registry"
    user = "${aws_iam_user.registry.name}"
    policy = <<EOF
{
  "Version": "2012-10-17",

 "Statement": [
        {
            "Effect": "Allow",
            "Action": "s3:ListAllMyBuckets",
            "Resource": "arn:aws:s3:::*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:ListBucket",
                "s3:GetBucketLocation"
            ],
            "Resource": "arn:aws:s3:::${var.bucket}"
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:PutObject",
                "s3:GetObject",
                "s3:DeleteObject"
            ],
            "Resource": "arn:aws:s3:::${var.bucket}/*"
        }
    ]
}
EOF
}

resource "aws_iam_user" "rexray" {
    name = "${var.stack_name}_rexray"
    path = "/${var.stack_name}/users/"
}

resource "aws_iam_access_key" "rexray_ak" {
    user = "${aws_iam_user.rexray.name}"
}

resource "aws_iam_user_policy" "rexray" {
    name = "rexray"
    user = "${aws_iam_user.rexray.name}"
    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "RexRayMin",
            "Effect": "Allow",
            "Action": [
                "ec2:AttachVolume",
                "ec2:CreateVolume",
                "ec2:CreateSnapshot",
                "ec2:CreateTags",
                "ec2:DeleteVolume",
                "ec2:DeleteSnapshot",
                "ec2:DescribeAvailabilityZones",
                "ec2:DescribeInstances",
                "ec2:DescribeVolumes",
                "ec2:DescribeVolumeAttribute",
                "ec2:DescribeVolumeStatus",
                "ec2:DescribeSnapshots",
                "ec2:CopySnapshot",
                "ec2:DescribeSnapshotAttribute",
                "ec2:DetachVolume",
                "ec2:ModifySnapshotAttribute",
                "ec2:ModifyVolumeAttribute",
                "ec2:DescribeTags"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
EOF
}

resource "aws_key_pair" "keypair" {
  key_name = "${var.stack_name}-keypair"
  public_key = "${file("./${var.stack_name}.keypair.pub")}"

   lifecycle {
      create_before_destroy = true
    }
}

#######################
## VPC PROVISIONNING ##
#######################
resource "aws_vpc" "default" {
  cidr_block = "${lookup(var.cidr_prefix, var.aws_region)}.0.0/16"
  enable_dns_hostnames = true
  tags {
     Name = "${var.stack_name}"
  }


   lifecycle {
      create_before_destroy = true
    }
}

resource "aws_internet_gateway" "default" {
  vpc_id = "${aws_vpc.default.id}"
  tags {
     Name = "${var.stack_name}"
  }
}

resource "aws_route53_zone" "zone" {
  name = "${var.stack_name}"
  vpc_id = "${aws_vpc.default.id}"

    tags  {
        Name = "${var.stack_name}-dns-zone"
        Stack = "${var.stack_name}"
        Type = "dns-zone"
        Id = "0"
    }
}

# Public subnets
resource "aws_subnet" "region-public-a" {
    vpc_id = "${aws_vpc.default.id}"

    cidr_block = "${lookup(var.cidr_prefix, var.aws_region)}.0.0/24"
    availability_zone = "${var.aws_region}${var.zone_1}"

  tags {
     Name = "${var.stack_name}"
  }
}
resource "aws_subnet" "region-public-b" {
    vpc_id = "${aws_vpc.default.id}"

    cidr_block = "${lookup(var.cidr_prefix, var.aws_region)}.2.0/24"
    availability_zone = "${var.aws_region}${var.zone_2}"

  tags {
     Name = "${var.stack_name}"
  }
}

# Routing table for public subnets
resource "aws_route_table" "region-public-a" {
    vpc_id = "${aws_vpc.default.id}"

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.default.id}"
    }
  tags {
     Name = "${var.stack_name}"
  }
}
resource "aws_route_table" "region-public-b" {
    vpc_id = "${aws_vpc.default.id}"

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.default.id}"
    }
  tags {
     Name = "${var.stack_name}"
  }
}

resource "aws_route_table_association" "region-public-a" {
    subnet_id = "${aws_subnet.region-public-a.id}"
    route_table_id = "${aws_route_table.region-public-a.id}"
}

resource "aws_route_table_association" "region-public-b" {
    subnet_id = "${aws_subnet.region-public-b.id}"
    route_table_id = "${aws_route_table.region-public-b.id}"
}

# Private subsets
resource "aws_subnet" "region-private-a" {
    vpc_id = "${aws_vpc.default.id}"

    cidr_block = "${lookup(var.cidr_prefix, var.aws_region)}.1.0/24"
    availability_zone = "${var.aws_region}${var.zone_1}"

  tags {
     Name = "${var.stack_name}"
  }

   lifecycle {
      create_before_destroy = true
    }
}

resource "aws_subnet" "region-private-b" {
    vpc_id = "${aws_vpc.default.id}"

    cidr_block = "${lookup(var.cidr_prefix, var.aws_region)}.3.0/24"
    availability_zone = "${var.aws_region}${var.zone_2}"
  tags {
     Name = "${var.stack_name}"
  }


   lifecycle {
      create_before_destroy = true
    }
}

# Routing table for private subnets
resource "aws_route_table" "region-private-a" {
    vpc_id = "${aws_vpc.default.id}"

    route {
        cidr_block = "0.0.0.0/0"
        instance_id = "${aws_instance.bastion.id}"
    }

    tags {
     Name = "${var.stack_name}"
    }
}
resource "aws_route_table" "region-private-b" {
    vpc_id = "${aws_vpc.default.id}"

    route {
        cidr_block = "0.0.0.0/0"
        instance_id = "${aws_instance.bastion.id}"
    }
  tags {
     Name = "${var.stack_name}"
  }
}

resource "aws_route_table_association" "region-private-a" {
    subnet_id = "${aws_subnet.region-private-a.id}"
    route_table_id = "${aws_route_table.region-private-a.id}"
}

resource "aws_route_table_association" "region-private-b" {
    subnet_id = "${aws_subnet.region-private-b.id}"
    route_table_id = "${aws_route_table.region-private-b.id}"
}

# Public
resource "aws_security_group" "public" {
    name = "public"
    description = "Allow traffic from the internet"
    vpc_id = "${aws_vpc.default.id}"
  tags {
     Name = "${var.stack_name}"
  }


   lifecycle {
      create_before_destroy = true
    }
}

resource "aws_security_group" "bastion" {
    name = "bastion"
    description = "Allow SSH traffic from the internet"

  vpc_id = "${aws_vpc.default.id}"
  tags {
     Name = "${var.stack_name}"
  }

}


resource "aws_instance" "bastion" {
    ami = "${var.aws_bastion_ami}"
    availability_zone = "${var.aws_region}${var.zone_1}"
    instance_type = "t2.small"
    key_name = "${var.stack_name}-keypair"
    security_groups = ["${aws_security_group.bastion.id}","${aws_security_group.public.id}"]
    subnet_id = "${aws_subnet.region-public-a.id}"
    source_dest_check = false
    user_data = <<EOF
#cloud-config
write_files:
  - path: "/etc/stack.conf"
    permissions: "0644"
    owner: "root"
    content: |
      STACK_NAME="${var.stack_name}"
      ADMIN_NETWORK="${lookup(var.cidr_prefix, var.aws_region)}.0.0/16"
      NAT_SUBNETS="${aws_subnet.region-private-a.cidr_block},${aws_subnet.region-private-b.cidr_block}"
EOF

    tags  {
        Name = "${var.stack_name}-bastion-a"
        Stack = "${var.stack_name}"
        Type = "bastion"
        Id = "a"
    }
}

resource "aws_eip" "bastion" {
    instance = "${aws_instance.bastion.id}"
    vpc = true
}

#Nodes
resource "aws_security_group" "nodes" {
    name = "nodes"
    description = "Allow SSH traffic from the Bastion"


    vpc_id = "${aws_vpc.default.id}"
    tags {
       Name = "${var.stack_name}"
    }
}

#Ext Nodes
resource "aws_security_group" "ext_nodes" {
  name = "ext_nodes"
  description = "Allow nodes traffic from external nodes"

  vpc_id = "${aws_vpc.default.id}"
  tags {
    Name = "${var.stack_name}"
  }
}

resource "aws_route53_record" "bastion_dns_records" {
    zone_id = "${aws_route53_zone.zone.zone_id}"
    name = "bastion.${var.stack_name}"
    type = "A"
    ttl = "30"
    records = ["${aws_instance.bastion.private_ip}"]
   lifecycle {
      create_before_destroy = true
   }
   depends_on = ["aws_security_group.nodes"]
}
