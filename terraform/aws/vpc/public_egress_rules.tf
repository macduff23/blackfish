resource "aws_security_group_rule" "public_to_nodes_tcp" {
    type = "egress"
    from_port = 32768
    to_port = 60999
    protocol = "tcp"

    security_group_id = "${aws_security_group.public.id}"
    source_security_group_id = "${aws_security_group.nodes.id}"
}
resource "aws_security_group_rule" "public_to_nodes_udp" {
    type = "egress"
    from_port = 32768
    to_port = 60999
    protocol = "udp"

    security_group_id = "${aws_security_group.public.id}"
    source_security_group_id = "${aws_security_group.nodes.id}"
}

resource "aws_security_group_rule" "public_to_nodes_web" {
  type = "egress"
  from_port = 80
  to_port = 80
  protocol = "tcp"

  security_group_id = "${aws_security_group.public.id}"
  source_security_group_id = "${aws_security_group.nodes.id}"
}

resource "aws_security_group_rule" "public_to_nodes_ssl" {
  type = "egress"
  from_port = 443
  to_port = 443
  protocol = "tcp"

  security_group_id = "${aws_security_group.public.id}"
  source_security_group_id = "${aws_security_group.nodes.id}"
}
